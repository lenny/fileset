#!/usr/bin/awk -f
BEGIN {
	FS="\t"
	current = "<unmarked>"
}
/^# FILE / {
	sub(/^# FILE +/, "")
	current = $0
}
/^\// {
	if($1 in fnames) {
		if(fnames[$1] != current) {
			dupes[$1] = dupes[$1] + 1
			fnames[$1] = fnames[$1] "\n" current
		}
	} else {
		fnames[$1] = current
	}
}
END {
	for(fname in dupes) {
		printf "duplicate filename %s found in:\n%s\n\n", fname, fnames[fname]
	}
}
