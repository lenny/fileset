|END {
/etc	d	+
/etc/env.d	d	+/***
/etc/env.d/20runit
C	SVDIR="/run/service/"

|portage_role_set("zsv-sysvinit", "app-shells/zsh sys-apps/sysvinit sys-process/runit")

/etc/runlevels	d	+/***
/etc/runlevels/boot	d
/etc/runlevels/default	d
/etc/sv.d	d	+/***
/etc/init.d	d	+/***
/etc/conf.d	d	+/***

/etc/sv.d/syslog-ng
C	logf -  # logs will go to supervisor
	prepare() {
		local -a vservers
		if [[ -d /etc/vservers ]]; then
			vservers=( /etc/vservers/*(N) )
			(($#vservers)) && mkdir -p /run/vservers-syslog/${^${vservers:t}}
		fi
	}
/etc/runlevels/boot/syslog-ng	r
/etc/runlevels/default/syslog-ng	r

/etc/runlevels/boot/zsv-logger	l	/etc/init.d/zsv-logger
/etc/runlevels/default/zsv-wait	l	/etc/init.d/zsv-wait

/etc/fstab	+
|line_append_file("mdev	","/dev","	tmpfs	noauto,exec,nosuid,mode=0755,size=10M	0 0", "^[^ 	#]+[	 ]", "[ 	]")

/etc/init.d/devfs	f	m755
/etc/init.d/mdev	f	m755
/etc/inittab	+
C	#
	# /etc/inittab:  This file describes how the INIT process should set up
	#                the system in a certain run-level.
	#
	# Author:  Miquel van Smoorenburg, <miquels@cistron.nl>
	# Modified by:  Patrick J. Volkerding, <volkerdi@ftp.cdrom.com>
	# Modified by:  Daniel Robbins, <drobbins@gentoo.org>
	# Modified by:  Martin Schlemmer, <azarah@gentoo.org>
	# Modified by:  Mike Frysinger, <vapier@gentoo.org>
	# Modified by:  Robin H. Johnson, <robbat2@gentoo.org>
	#
	# $Header: /var/cvsroot/gentoo-x86/sys-apps/sysvinit/files/inittab-2.87,v 1.2 2013/04/20 03:51:26 vapier Exp $
	
	# Default runlevel.
	id:3:initdefault:
	
	# System initialization, mount local filesystems, etc.
	si::sysinit:/sbin/rc sysinit
	
	# for rescue login moved up here
	c2:2345:respawn:/sbin/agetty 38400 tty2 linux
	# runit's runsvdir; services are selectively added
	
	# in boot and default runlevels via openrc and zsvgen
	ru:2345:respawn:/usr/local/bzr/zsv/sbin/runsvdirboot --default down -u syslog-ng
	
	# Further system initialization, brings up the boot runlevel.
	# changed from bootwait so above will run in parallel
	rc::wait:/sbin/rc boot
	
	l0:0:wait:/sbin/rc shutdown
	l0s:0:wait:/sbin/halt -dhp
	l1:1:wait:/sbin/rc single
	l2:2:wait:/sbin/rc nonetwork
	l3:3:wait:/sbin/rc default
	l4:4:wait:/sbin/rc default
	l5:5:wait:/sbin/rc default
	l6:6:wait:/sbin/rc reboot
	l6r:6:wait:/sbin/reboot -dk
	#z6:6:respawn:/sbin/sulogin
	
	# make sure zsv services were generated
	# should be done by openrc scripts, but nothing bad will happen
	# by running this here once again
	zsv:345:wait:/usr/local/bzr/zsv/sbin/zsvgen --all-up
	
	# new-style single-user
	su0:S:wait:/sbin/rc single
	su1:S:wait:/sbin/sulogin
	
	# TERMINALS
	c1:12345:respawn:/sbin/agetty --noclear 38400 tty1 linux
	c3:2345:respawn:/sbin/agetty 38400 tty3 linux
	c4:2345:respawn:/sbin/agetty 38400 tty4 linux
	c5:2345:respawn:/sbin/agetty 38400 tty5 linux
	c6:2345:respawn:/sbin/agetty 38400 tty6 linux
	
	# SERIAL CONSOLES
|if(<serial_terminal:>) {
	s0:12345:respawn:/sbin/agetty -L 115200 ttyS0 {<serial_terminal>}
|} else {
	#s0:12345:respawn:/sbin/agetty -L 115200 ttyS0 vt100
|}
	#s1:12345:respawn:/sbin/agetty -L 115200 ttyS1 vt100
	
	# What to do at the "Three Finger Salute".
	ca:12345:ctrlaltdel:/sbin/shutdown -r now
	
	# Used by /etc/init.d/xdm to control DM startup.
	# Read the comments in /etc/init.d/xdm for more
	# info. Do NOT remove, as this will start nothing
	# extra at boot if /etc/init.d/xdm is not added
	# to the "default" runlevel.
	x:a:once:/etc/X11/startDM.sh
m644
|}